package app.saby.gettheemail

data class Weather(
    val id: String = "",
    val title: String = "",
    val temp: Int? = null,
    val image_url: String = "",
    val secret_code: String? = null,
    val timeStamp: Long? = null
)