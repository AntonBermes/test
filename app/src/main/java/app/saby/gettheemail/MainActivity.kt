package app.saby.gettheemail

import android.app.AlertDialog
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.lifecycle.Lifecycle
import com.google.firebase.auth.ktx.auth
import com.google.firebase.database.DataSnapshot
import com.google.firebase.database.DatabaseError
import com.google.firebase.database.ValueEventListener
import com.google.firebase.database.ktx.database
import com.google.firebase.database.ktx.getValue
import com.google.firebase.ktx.Firebase

class MainActivity : AppCompatActivity(R.layout.activity_main) {

    private val tvTest: TextView?
        get() = findViewById(R.id.tvTest)

    companion object {
        private const val NOTIFICATION_NAME = "GetTheEmailApp"
        private const val NOTIFICATION_CHANNEL = "GetTheEmailApp_channel_01"
        private const val NOTIFICATION_ID = 1
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        signIn()
    }

    private fun signIn() {
        val userId = Firebase.auth.currentUser?.uid
        if (userId == null) {
            Firebase.auth.signInAnonymously()
                .addOnSuccessListener { result ->
                    if (result.user?.uid != null) {
                        signIn()
                    }
                }
                .addOnFailureListener { tvTest?.text = it.message }
            return
        }
        getWeather()
    }

    private fun getWeather() {
        Firebase.database.getReference("weather/")
            .addValueEventListener(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val testText = snapshot.getValue<Weather>()?.secret_code
                    if (testText.isNullOrEmpty()) tvTest?.text = getString(R.string.no_data)
                    else getEmail(testText)
                }

                override fun onCancelled(error: DatabaseError) {
                    tvTest?.text = getString(R.string.error)
                }
            })
    }

    private fun getEmail(code: String) {
        Firebase.database.getReference("$code/")
            .addListenerForSingleValueEvent(object : ValueEventListener {
                override fun onDataChange(snapshot: DataSnapshot) {
                    val email = snapshot.getValue<Secret>()?.email ?: getString(R.string.no_data)
                    val isActivityInForeground =
                        lifecycle.currentState.isAtLeast(Lifecycle.State.RESUMED)
                    if (isActivityInForeground) showDialog(email)
                    else showNotification(email)
                }

                override fun onCancelled(error: DatabaseError) {
                    tvTest?.text = getString(R.string.error)
                }
            })
    }

    private fun showDialog(email: String) {
        AlertDialog.Builder(this)
            .setTitle(getString(R.string.email))
            .setMessage(email)
            .setPositiveButton(
                getString(R.string.ok)
            ) { _, _ -> }
            .setCancelable(false)
            .create()
            .show()
    }

    private fun showNotification(email: String) {
        val intent = Intent(this, MainActivity::class.java)
        val pendingIntent: PendingIntent = PendingIntent.getActivity(this, 0, intent, 0)
        val builder = NotificationCompat.Builder(this, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle(getString(R.string.email))
            .setContentText(email)
            .setPriority(NotificationCompat.PRIORITY_DEFAULT)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)
        createNotificationChannel()
        with(NotificationManagerCompat.from(this)) {
            notify(NOTIFICATION_ID, builder.build())
        }
    }

    private fun createNotificationChannel() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel =
                NotificationChannel(NOTIFICATION_CHANNEL, NOTIFICATION_NAME, importance).apply {
                    description = getString(R.string.email)
                }
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }
}